# -*- coding: utf-8 -*-
from flask import Flask, render_template, url_for, redirect, request, flash
from flask_admin import Admin, expose, AdminIndexView
from flask_admin.actions import action
from flask_admin.contrib.mongoengine import ModelView
from mongoengine import connect
import wtforms as f


from models import Student, first_setting

# app configures
app = Flask(__name__)
connect('test')


class IndexView(AdminIndexView):
    @expose('/')
    def index(self):
        return self.render('/index.html')


class MyForm(f.Form):
    full_name = f.StringField(label=u'Повне імʼя')
    full_name_d = f.StringField(label=u'Повне імʼя в давальному')
    full_name_r = f.StringField(label=u'Повне імʼя родовому')


class StudentView(ModelView):
    @action('my_form', 'my_form')
    def my_form(self, ids):
        return redirect(url_for('.change_name', ids=','.join(ids)))

    @expose('/change_name', methods=('POST', 'GET'))
    def change_name(self):
        # list of student id
        ids = request.args['ids'].split(',')
        print ids

        # our form
        form = MyForm(request.form)

        if request.method == 'POST':
            # getting data
            fn, fn_d, fn_r = form.full_name.data, form.full_name_d.data, form.full_name_r.data
            print fn
            print fn_d
            print fn_r

            # retrieving queryset of students with our ids
            queryset = self.model.objects(id__in=ids)
            print queryset

            # updating students
            queryset.update(set__full_name=fn, set__full_name_d=fn_d, set__full_name_r=fn_r)

            flash(u'Студентам встановлено імʼя "{}"'.format(fn), 'info')
            return redirect(url_for('.index_view'))

        # rendering form
        return self.render('form.html', form=form, send_url=url_for('.change_name'))


admin = Admin(app, index_view=IndexView(url='/'), base_template='admin/base.html')
admin.add_view(StudentView(model=Student))


if __name__ == '__main__':
    Student.drop_collection()
    first_setting()
    app.run('0.0.0.0', port=5000)

