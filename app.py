# -*- coding: utf-8 -*-
from flask import Flask, render_template
from mongoengine import connect
from views import order_academ_ended, order_academ_go_on, order_academ_give


# app configures
app = Flask(__name__)
connect('test')


# Student.objects()[:].delete()
# first_setting()


# main page with list
@app.route('/')
def main():
    return render_template('main.html')


# all urls
app.add_url_rule('/order_academ_ended', view_func=order_academ_ended)
app.add_url_rule('/order_academ_go_on', view_func=order_academ_go_on)
app.add_url_rule('/order_academ_give', view_func=order_academ_give)


if __name__ == '__main__':
    app.run()

